import { Component, OnInit } from '@angular/core';
import { TodoService } from '../../services/todo.service';
import { ActivatedRoute } from '@angular/router';
import { Lista } from '../../models/lista.model';
import { ListaItem } from '../../models/lista-items.model';

@Component({
  selector: 'app-agregar',
  templateUrl: './agregar.page.html',
  styleUrls: ['./agregar.page.scss'],
})
export class AgregarPage implements OnInit {

  lista: Lista;
  nombreItem = '';


  constructor(private toDoService: TodoService, private activatedRoute: ActivatedRoute) {
    const listaId = this.activatedRoute.snapshot.paramMap.get('listaId');

    this.lista = this.toDoService.obtenerLista(listaId);
    
  }

  ngOnInit() {
  }

  agregarItem() {
    if (this.nombreItem.length === 0) {
      return;
    }

    const nuevoItem = new ListaItem(this.nombreItem);

    this.lista.items.push(nuevoItem);
    this.nombreItem = '';

    this.toDoService.guardarStorage();
  }

  cambioCheck(item: ListaItem) {

    const pendientes = this.lista.items.filter( itemData => !itemData.completado).length;

    if (pendientes === 0) {
      this.lista.terminadaEn = new Date();
      this.lista.completada = true;
    } else {
      this.lista.terminadaEn = null;
      this.lista.completada = false;
    }

    this.toDoService.guardarStorage();
  }

  eliminarItem(i: number) {
    this.lista.items.splice(i, 1);
    this.toDoService.guardarStorage();
  }

}
