export class ListaItem {
    des: string;
    completado: boolean;

    constructor( desc: string){
        this.des = desc;
        this.completado = false;
    }
}