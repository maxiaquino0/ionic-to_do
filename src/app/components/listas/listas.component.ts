import { Component, OnInit, Input, ViewChild } from '@angular/core';
import { Lista } from '../../models/lista.model';
import { Router } from '@angular/router';
import { TodoService } from '../../services/todo.service';
import { AlertController, IonList } from '@ionic/angular';

@Component({
  selector: 'app-listas',
  templateUrl: './listas.component.html',
  styleUrls: ['./listas.component.scss'],
})
export class ListasComponent implements OnInit {

  @ViewChild( IonList ) lista: IonList;
  @Input() terminada = true;

  constructor(public todoService: TodoService, private router: Router,
              private alertController: AlertController) {}

  ngOnInit() {}

  lisatSeleccionada(lista: Lista) {
    if (this.terminada) {
      this.router.navigateByUrl(`tabs/tab2/agregar/${ lista.id }`);
    } else {
      this.router.navigateByUrl(`tabs/tab1/agregar/${ lista.id }`);
    }
  }

  eliminarLista(lista: Lista) {
    this.todoService.eliminarLista(lista);
  }

  async editarNombreLista(lista: Lista) {
    const alert = await this.alertController.create({
      header: 'Cambiar nombre lista',
      inputs: [
        {id: 'titulo', name: 'titulo', type: 'text', value: lista.titulo}
      ],
      buttons: [
        {
          text: 'Cancelar',
          role: 'cancel',
          handler: () => {
            console.log('Cancelar');
            this.lista.closeSlidingItems();
          }
        },
        {
          text: 'Cambiar',
          handler: (data) => {
            console.log(data);
            if (data.titulo.length === 0) {
              return;
            }
            // modificamos la lista
            lista.titulo = data.titulo;
            this.todoService.guardarStorage();
            this.lista.closeSlidingItems();
          }
        }
      ]
    });

    alert.present().then( () => {
      document.getElementById('titulo').focus();
    });
  }

}
